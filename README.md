# ssh-build-config

This script is intended to solve the issue in ssh about the lack of support for an "include" directive in the ~/.ssh/config file

## Install

* Install python3
* Clone this repository to a local directory (for example to /opt/)
* Add the directory to the `$PATH` (optional to run the command without de full path)
* Make `~/.ssh/config.d/` directory (`mkdir ~/.ssh/config.d/`)
* Move `~/.ssh/config` to a file inside `~/.ssh/config.d/` with the right name (example: `mv ~/.ssh/config ~/.ssh/config.d/00_my_config.conf`)
* run `ssh-build-config` (if you haven't changed the `$PATH` is needed the full path)

After any change in the `~/.ssh/config.d/` you have tu run the command

## ~/.ssh/config.d/

The script will search every file and directory recursively.

The name must be `[0-9]{2}_.*\.conf`

For example:

```
├── 01_my_global_stuff.conf
├── 02_my_hosts.conf
├── 03_my_datacenter.conf
│   ├── 01_web_servers.conf
│   └── 02_db_servers.conf
└── 99_default.conf

```

## Config

All configs for this script are stored in `~/.ssh/ssh-build-config/`

### usernames

Inside the config dir you can place the `usernames` file

The format is

```
placeholder1=username1
placeholder2=username2
```

during the `ssh-build-config` execution all placeholders will be replaced with the username.
